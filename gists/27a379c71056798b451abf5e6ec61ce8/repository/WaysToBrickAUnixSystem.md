<img 
     align="right" width="200px"
     src="https://user-images.githubusercontent.com/90007553/167656464-c1751872-2ebb-4fa2-91a9-7f3c2c8036ec.png"
/>

# Ways To Not Use Your Unix Machine 

(Run these as root :trollface:)

### `rm` Remove Command 

**All commands delete the Root in different ways**

- The Classic 

```bash 
rm -rf / --no-preserve-root
```

- All files except root

```bash
rm -rf /*
```

- Something more Elaborate

```bash
cd / && find /* | xarg rm -rf
```

> "If you're gonna delete your filesystem, at least lay pipes and arguments like a Linux chad."

<br />

### `dd` Disk Destroyer Command

- The Classic
- **NOTE: `root` may not be your root, check your Operating System's root.**

```bash
dd if=/dev/null of=/root
```

- Overriding Ram
- **NOTE: `/dev/mem` does not exist on most Kernel configurations.**

```bash
dd if=/dev/urandom of=/dev/mem
```

- Writing Random Files to your disk
- **NOTE: `/dev/sda` may not be your disk, it can be `hdX`, `sdX`, or `nvme0nX`**

```bash
dd if=/dev/random of=/dev/sda
```

- Cleaning all Partitions on a Disk

```bash
dd if=/dev/zero of=/dev/sda  bs=512  count=1
```

<br />

### Some More Elaborate Ways

- Fork Bombing

```bash
:(){ :|: & };:
```

<details>
   <summary>How This Works</summary>
     
   <br />
     
   > :() means you are defining a function called
   > {:|: &} means run the function : and send its output to the : function again and run that in the background.
: – load another copy of the ‘:’ function into memory | – and pipe its output to : – another copy of ‘:’ function, which has to be loaded into memory Therefore, ‘:|:’ simply gets two copies of ‘:’ loaded whenever ‘:’ is called & – disown the functions, if the first ‘:’ is killed, all of the functions that it has started should NOT be auto-killed } – end of what to do when we say ‘:’
   >; Command Seperator
   >: runs the function first time
     
</details>

- Destroying your Home File System

```bash
mv ~ /dev/null
```

> This moves your home directory to the `null` directory, where it is disposed of.

- `shred`, Might Take a While

```bash
shred -n 5 -vz /dev/sda
```

> Shred is a GNU command which deletes the contents of your disk in 7 rounds, so that it can't be recovered.

- If an Arch user is getting a little annoying, run this on their computer

```bash
sudo pacman -Rcs systemd
```

- For Grub Users

```bash
rm -r /boot/grub/grub.cfg
```

> Removes the Grub config file, this prevents you from booting into Linux

- Reformatting your Drive

```bash
mkfs.ext4 /dev/sda 
```

> Formats and deletes the data on your disks parition.

- Black Hole?

```bash
mv / /dev/null
```

> Moves your root directory to `null` were it is disposed of. 

- Delete your Super User

```bash
rm -f /usr/bin/sudo ; rm -f /bin/su
```

> Removing your super user, prevents you from doing super user tasks.

<br />

## How Not To Program

### Assembly Languages

- **Netwide Assembly Language (NASM)**

```asm
bits 64

global _start

section .text

_start:
        mov    rax, strict 0x39     ; 'stub_fork' syscall.
        syscall                     ; Calling 'stub_fork'
        jmp    short _start         ; Looping this procedure.
```

- **X86 Flat Assembler (FASM)**

```asm
format ELF executable
entry start
start:
```

- **GNU Assembler (GAS)**

```gas
.globl _start

.text

_start: 
        mov    $0x39, %rax   # 'stub_fork' syscall.
        syscall              # Calling 'stub_fork'
        jmp    .             # Looping to the start of this procedure.
```

> All segments only work on Linux

<img 
     align="right" width="30"
     src="https://bashlogo.com/img/symbol/png/full_colored_light.png"
/>
     
### Bash

```bash
#!/bin/bash

func() { 
    func | func
    & 
}; func
```

> This is the equivalent of `:(){ :|: & };:`

### [BASIC For the Commodore PET 2001](https://en.wikipedia.org/wiki/Killer_poke)

```basic
POKE 59458,62
```

<img 
     align="right" width="30"
     src="https://upload.wikimedia.org/wikipedia/commons/1/19/C_Logo.png"
/>
### C for Unix

```C
#include <unistd.h>
 
int main(void) {
    /* Unconditional for loop, loops forever. */
    for(;;)
        fork();
        
    return 0;
}
```

### C++ Unix
```C
#include <thread>
 
int main(void) {
    for(;;) {
        std::thread([]() -> void {
            for(;;) {}
        });
    }
    return 0;
}
```

<img 
     align="right" height="20"
     src="https://upload.wikimedia.org/wikipedia/commons/0/05/Go_Logo_Blue.svg"
/>
### Go

```go
package main

func main() {
    for {
        go main()
    }
}
```

<img
     align="right" width="30"
     src="https://user-images.githubusercontent.com/90007553/167705503-9b51fcc3-d06f-479c-83cf-d704878ad0a7.png"
/>
### Haskell

```hs
import Control.Concurrent

f = do a <- forkIO g
       b <- forkIO g
       return ()

g = do a <- forkIO f
       b <- forkIO f
       return ()
       
main = do f 
```

### Java

```java
public class Main { 

    public static void Main(String[] args) {
        while (true) {
            Runtime.getRuntime().exec(new String[] {
                "javaw", "-cp", System.getProperty("java.class.path"), "Main"
            });
        }
    }
}
```

- If you came to Java for the coffee beans, you can roast them with this method.

```java
import java.lang.Thread;

public class Heater { 

    public static void Main(String[] args) {
        for (;;) {
            new Thread(() -> {
                while (true);
            }).start();
        }
    }
}
```

### Kotlin

```kt
fun main() {
    while (true) {
        Thread {
            while (true);
        }.start()
    }
}
```

### Python

```py
import os

while 1:
     os.fork()
```

### PHP (Poor Helpless Programmers)

```php
while(pcntl_fork()|1);
```

### Ruby

```ruby 
loop { fork }
```

### Swift

```swift
import Darwin

class main {
    while true {
        _ = unsafeBitCast (
            dlsym (
                dlopen (nil, RTLD_NOW),
                "fork"
            ),
            to: (@convention(c) () -> pid_t).self
        ) ()
    }
}
```

> For OS/X and derivatives

<!--
- Crash a JVM

```java
public class Main {
    public static void main(String[] args) {
        Object[] o = null;

        while (true) {
            o = new Object[] {o};
        }
    }
}
```

- Memory Leakage 1

```java
try {
    BufferedReader br = new BufferedReader(new FileReader(inputFile));
    ...
    ...
} catch (Exception e) {
    e.printStacktrace();
}
```

- Memory Leak 2

```java
class Example {
   public final String key;
   public BadKey(String key) { this.key = key; }
}

Map map = System.getProperties();
map.put(new BadKey("key"), "value"); // Memory leak even if your threads die.
```
-->

<!-- 
### Integer Overflow's in C

```C
char *buf;
int i, len;

read(fd, &len, sizeof(len));

/* I forgot to check the output of < 0, oh no! */
if (len > 8000) { error("too large length"); return; }

buf = malloc(len);
read(fd, buf, len);
```

```C
/* if "unsigned int" is 32 bits and "size_t" is 64 bits: */
 
void *mymalloc(unsigned int size) { return malloc(size); }
 
char *buf;
size_t len;
 
read(fd, &len, sizeof(len));
 
/* 64-bit size_t gets truncated to 32-bit unsigned int */
buf = mymalloc(len);
read(fd, buf, len);
```

```C
char *buf;
size_t len;
 
read(fd, &len, sizeof(len));
 
buf = malloc(len+1); /* +1 can overflow to malloc(0) */
read(fd, buf, len);
buf[len] = '\0';
```
-->

<br />

<details>
  <summary>Honorable Mention</summary>
  
  <br />
  
  <img 
       src="https://cdn.discordapp.com/attachments/916668445104427068/922828998336217088/iu.png"
  />
  
</details>