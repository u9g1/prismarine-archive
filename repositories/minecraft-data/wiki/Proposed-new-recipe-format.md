## Problems with the old format

The [old format](Recipe-format) has some design flaws, which prevent it to be updated to newer Minecraft versions.  
This new format adresses these issues and tries to still be manually editable.  

### Metadata in the shapes

Both Stonebricks and Smooth Granite use a 2x2 of Stone (ID=1), however depending on the metadata, they use two different recipes: (1:0 as in id:meta)
- 2x2 of 1:0 for Stonebricks,
- 2x2 of 1:1 for Polished Granite.

There is currently no way to represent this difference.

### Inconsistent item format

The resulting item is given as a dictionary of `type`, `metadata`, and `count`, along with the shape or ingredients for how to craft it.  
`ingredients` consists of a dictionary with only `id` and `metadata`.  
`inShape` and `outShape`'s rows contain only the item ID as simple integer values.  
The crafted item is given as several attributes of the recipe.  

## New format

### Items

An item can be represented different ways.  

A single **numerical ID** or `null`.  
**Examples:**  
- `1` for any item with ID `1`
- `null` for empty

A **list** of `id` and `meta`.  
This is preferred if there are many items at once, e.g. in a shape.  
**Example:** `[1, 2]` for any amount of Polished Granite  

A **dictionary** of at least `id`, optionally `meta` and `amount`.  
This is preferred if there are not many items at once, e.g. `result` in a recipe.  
**Examples:**  
- `{"id": 1}` for any item with ID `1`
- `{"id": 1, "meta": 3}` for any amount of Diorite
- `{"id": 1, "meta": 2, "amount": 4}` for 4 Polished Granite

### Shapes

A shape is a list of rows, which are lists of items.  
There must be at least one row with at least one item in it.  
All rows must have the same length.  
Empty rows at the beginning or end of a shape may be omitted.  
Empty colums at the end may also be omitted.  
When an item can be crafted in a 2x2 grid, the shape may not be larger than 2x2.  

**Examples:**

Stick:

	[
		[5],
		[5]
	]
	or
	[
		[5, null],
		[5, null]
	]

Stonebricks:

	[
		[1, 1],
		[1, 1]
	]

Polished Granite:

	[
		[[1,1], [1,1]],
		[[1,1], [1,1]]
	]
	or
	[
		[{"id": 1, "meta": 1}, {"id": 1, "meta": 1}],
		[{"id": 1, "meta": 1}, {"id": 1, "meta": 1}]
	]

### Recipes

A shaped recipe is a dictionary of `result`, `inShape` and optionally `outShape`.  
A shapeless recipe is a dictionary of `result` and `ingredients`.  
`inShape` and `outShape` are shapes, as specified [above](#Shapes).  
`ingredients` is a list of items.  
`result` is a single item.  
Items may be in any of the representations [above](#Items).  
`outShape` must be of the same dimensions as `inShape`.  

### File format

The file is in JSON format.  
At the top level is a dictionary of quoted numerical item IDs.  
Each ID maps to a list of recipes.  
There may be multiple different recipes per item (same ID and meta).  
The recipes may not be sorted.  

**Example:**

```js
{
	"1": [
		{ // Polished Granite
			"result": {"id": 1, "meta": 2, "amount": 4},
			"inShape": [
				[[1,1], [1,1]],
				[[1,1], [1,1]]
			]
		},
		{ // Polished Diorite
			"result": {"id": 1, "meta": 4, "amount": 4},
			"inShape": [
				[[1,3], [1,3]],
				[[1,3], [1,3]]
			]
		}
	],
	"5": [
		{
			"result": {"id": 5, "meta": -1, "amount": 4},
			"inShape": [
				[17]
			]
		}
	],
	"280": [
		{
			"result": {"id": 280, "meta": -1, "amount": 4},
			"inShape": [
				[5],
				[5]
			]
		}
	],
	"354": [ // cake
		{
			"result": 354,
			"inShape": [
				[335, 335, 335],
				[353, 344, 353],
				[296, 296, 296]
			],
			"outShape": [
				[ 325,  325,  325],
				[null, null, null],
				[null, null, null]
			]
		}
	],
	"378": [ // magma cream
		{
			"result": 378,
			"ingredients": [341, 377]
		}
	]
}
```

## Example implementations

- [Python](https://gist.github.com/Gjum/9c0491aad1c8ec8d6f38#file-recipe-py)

## Json schema
The [json schema](https://github.com/PrismarineJS/minecraft-data/blob/master/enums_schemas/recipes_schema.json) automatically checks whether the recipes.json respects that format, see the [visualization](http://prismarinejs.github.io/minecraft-data/) here.